import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TranslationsComponent} from './components/translations/translations.component';

const routes: Routes = [
  { path: '', redirectTo: '/translations', pathMatch: 'full' },
  { path: 'translations', component: TranslationsComponent },
  { path: '**', redirectTo: '/translations' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
