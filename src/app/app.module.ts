import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {MaterialModule} from './shared/material.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { TranslationsService } from './services/translations.service';
import {CustomHttpInterceptor} from './services/interceptors/http.interceptor';
import {TranslationsComponent} from './components/translations/translations.component';
import { TranslationDialogComponent } from './components/translation-dialog/translation-dialog.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { TestingDialogComponent } from './components/testing-dialog/testing-dialog.component';
import {FormService} from './services/form.service';
import {MathService} from './services/math.service';

@NgModule({
  declarations: [
    AppComponent,
    TranslationsComponent,
    TranslationDialogComponent,
    TestingDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    TranslationDialogComponent,
    TestingDialogComponent
  ],
  providers: [
    TranslationsService,
    FormService,
    MathService,
    { provide: HTTP_INTERCEPTORS, useClass: CustomHttpInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
