import { NgModule } from '@angular/core';

import {
  MatButtonModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatStepperModule,
  MatSnackBarModule,
  MatTableModule
} from '@angular/material';

const arr = [
  MatButtonModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatStepperModule,
  MatSnackBarModule,
  MatTableModule
];


@NgModule({
  exports: [
    ...arr
  ]
})
export class MaterialModule { }
