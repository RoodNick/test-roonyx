import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {appConfig} from '../app.config';
import {Observable} from 'rxjs/Observable';
import {TranslationPair} from '../models/translation-pair.model';

@Injectable()
export class TranslationsService {

  constructor(private http: HttpClient) { }

  getTranslations(): Observable<TranslationPair[]> {
    return this.http.get<TranslationPair[]>(appConfig.apiUrl);
  }

  addTranslation(translation: TranslationPair): Observable<Object> {
    return this.http.post(appConfig.apiUrl, translation);
  }

  editTranslation(translation: TranslationPair): Observable<Object> {
    return this.http.put(appConfig.apiUrl + `/${translation.id}`, translation);
  }

}
