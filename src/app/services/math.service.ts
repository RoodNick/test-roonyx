import { Injectable } from '@angular/core';

@Injectable()
export class MathService {

  static random(max: number): number {
    return Math.floor(Math.random() * max);
  }

  static randomExcluded(excluded: number, max: number): number {
    const value = this.random(max);
    return value >= excluded ? value + 1 : value;
  }

  constructor() { }

}
