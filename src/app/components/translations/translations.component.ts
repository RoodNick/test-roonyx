import { Component, OnInit } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {TranslationPair} from '../../models/translation-pair.model';
import {TranslationsService} from '../../services/translations.service';
import {Observable} from 'rxjs/Observable';
import {DataSource} from '@angular/cdk/collections';
import {MatDialog} from '@angular/material';
import {TranslationDialogComponent} from '../translation-dialog/translation-dialog.component';
import 'rxjs/add/operator/finally';
import 'rxjs/add/operator/do';
import {TestingDialogComponent} from '../testing-dialog/testing-dialog.component';
import {MathService} from '../../services/math.service';

@Component({
  selector: 'app-translations',
  templateUrl: './translations.component.html',
  styleUrls: ['./translations.component.styl']
})
export class TranslationsComponent implements OnInit {
  loading = false;

  testSize = 20;

  dataSource: CustomDataSource | null;

  dataSubject = new BehaviorSubject<TranslationPair[]>([]);

  displayedColumns = ['name', 'translation', 'edit'];

  constructor(private translationsService: TranslationsService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.loading = true;
    this.translationsService.getTranslations()
      .subscribe(
        (translations: TranslationPair[]) => {
            this.dataSubject.next(translations);
            this.loading = false;
        },
        (error) => {
          console.log('ошибка загрузки данных');
        });
    this.dataSource = new CustomDataSource(this.dataSubject);
  }

  addTranslation() {
    this.loading = true;
    const dialogRef = this.dialog.open(TranslationDialogComponent, {
      data: {
        title: 'Добавление перевода',
        approve: 'Добавить',
        name: '',
        translation: ''
      }
    });

    dialogRef.afterClosed().subscribe(translationPair => {
      if (translationPair) {
        this.translationsService.addTranslation(translationPair)
          .subscribe((success) => {
            const tableData: TranslationPair[] = this.dataSubject.value;
            tableData.push(translationPair);
            this.dataSubject.next(tableData);
            this.loading = false
          },
          (error) => {
            this.loading = false;
            console.log('ошибка добавки');
          });
      }
    });
  }

  editTranslation(currentTranslationPair: TranslationPair, index: number) {
    this.loading = true;
    const dialogRef = this.dialog.open(TranslationDialogComponent, {
      data: {
        title: 'Редактирование перевода',
        approve: 'Сохранить',
        translationPair: currentTranslationPair
      }
    });

    dialogRef.afterClosed().subscribe(translationPair => {
      if (translationPair) {
        this.translationsService.editTranslation(translationPair)
          .subscribe((success) => {
              const tableData: TranslationPair[] = this.dataSubject.value;
              tableData[index] = translationPair;
              this.dataSubject.next(tableData);
              this.loading = false
            },
            (error) => {
              console.log('ошибка редактирования');
              this.loading = false
            });
      }
    });
  }

  startTest() {
    const randomIndex: number = MathService.random(this.dataSource.length - this.testSize);

    const dialogRef = this.dialog.open(TestingDialogComponent, {
      width: '400px',
      //  height: '480px',
      data: {
        title: 'Тестирование',
        variants: this.dataSubject.value.slice(randomIndex, randomIndex + this.testSize)
      }
    });
  }

}

export class CustomDataSource extends DataSource<TranslationPair> {
  length = 0;

  constructor(private data: BehaviorSubject<TranslationPair[]>) {
    super();
  }

  connect(): Observable<TranslationPair[]> {
    return this.data.asObservable().do(data => {
      if (data) {
        this.length = data.length;
      }});
  }

  disconnect(): void {

  }

}
