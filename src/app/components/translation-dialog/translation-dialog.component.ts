import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-translation-dialog',
  templateUrl: './translation-dialog.component.html',
  styleUrls: ['./translation-dialog.component.styl']
})
export class TranslationDialogComponent implements OnInit {
  title = '';
  approve = '';

  form: FormGroup;

  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<TranslationDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any) {

  }

  ngOnInit() {
    this.title = this.data.title;
    this.approve = this.data.approve;
    this.form = this.fb.group({
      name: [this.data.translationPair ? this.data.translationPair.name : '', [Validators.required]],
      translation: [this.data.translationPair ? this.data.translationPair.translation : '', [Validators.required]],
    });
  }

  onSubmit(form) {
    const translationPair = this.data.translationPair;
    translationPair.name = form.value.name;
    translationPair.translation = form.value.translation;
    this.dialogRef.close(translationPair);
  }
}
