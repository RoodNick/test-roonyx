import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {MathService} from '../../services/math.service';
import {FormService} from '../../services/form.service';

@Component({
  selector: 'app-testing-dialog',
  templateUrl: './testing-dialog.component.html',
  styleUrls: ['./testing-dialog.component.styl']
})
export class TestingDialogComponent implements OnInit {
  title = '';

  hide = false;
  loading = false;

  step = 0;
  result;

  radioSize = 6;

  randoms: number[] = [];

  form: FormGroup;
  variants: FormArray;

  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<TestingDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.loading = true;
    this.title = this.data.title;

    this.randomizeRadio();

    this.variants = this.fb.array([]);
    for (let index = 0; index < this.data.variants.length; index++) {
      this.variants.push(this.fb.group({
        translate: ['', Validators.required]
      }, {
        validator: (formGroup: FormGroup) => {
          return FormService.validateGroup(formGroup);
        }
      }));
    }

    this.form = this.fb.group({
      variants: this.variants
    });

    this.loading = false;
  }

  nextStep() {
    this.hide = true;
    this.step++;
    this.randomizeRadio();
    this.hide = false;
  }

  randomizeRadio() {
    for (let index = 0; index < this.radioSize; index++) {
      this.randoms[index] = MathService.randomExcluded(this.step, this.data.variants.length - 2);
    }
    this.randoms[MathService.random(this.radioSize - 1)] = this.step;
  }

  onSubmit(form) {
    this.loading = true;
    let result = 0;
    for (let index = 0; index < this.variants.length; index++) {
      if (this.variants.at(index).get('translate').value === this.data.variants[index].translation) {
        result++;
      }
    }
    setTimeout(() => {
      this.result = result;
      this.loading = false;
    }, 2000);
  }
}
