export interface TranslationPair {
  id: number;
  name: string;
  translation: string;
}
