import { TestRoonyxPage } from './app.po';

describe('test-roonyx App', () => {
  let page: TestRoonyxPage;

  beforeEach(() => {
    page = new TestRoonyxPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
